# README
https://bitbucket.org/tutorials/markdowndemo
## Generating token secret
node 
require('crypto').randomBytes(64).toString('hex')
## Things to be proud of
* Used projection in userProvider.js to hide password in API response
* async/await and promise constructions is used instead of callbacks
* "..." syntax
## TODO
* Checking models in separate func
* Checking user in DB before hash password
## Ideas
### General
* Check if mongoDB works with async/await syntax if I recreate client variable every time (closing connection = delete client var)
* Managing DB in separate class
* MongoDB Models?
* If ops are normal users with perms variable set to 'op', it's necessary to have addicional api to ops?
* insertUser could be simpler
### If I have time
* Typescript?

