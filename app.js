const express = require('express');
const colors = require('colors');

require('dotenv').config();

const { UserManager } = require('./services/userManager');
const { RoomManager } = require('./services/roomManager');

const app = express();

app.use(express.json());

var userManager = new UserManager(app);
var roomManager = new RoomManager(app);

app.get('/', (request, response) => {
    response.send('Hello world!')
});

app.listen(3000, () => {
    console.log("App started".green);
});