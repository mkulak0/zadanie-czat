const { RoomProvider } = require("./roomProvider");

RoomManager = function(app) {

    var roomProvider = new RoomProvider();
    
    app.put('/rooms', (req, res) => {
        roomProvider.insertRoom(req.body);
    });

}

exports.RoomManager = RoomManager;