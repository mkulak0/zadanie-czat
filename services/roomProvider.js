const { MongoClient } = require('mongodb');
// const client = new MongoClient(process.env.DB_URL, { useUnifiedTopology: true });

/* Called only for 'users' not 'ops'! */
async function isUserHaveARoom(userId){
    let client = new MongoClient(process.env.DB_URL, { useUnifiedTopology: true });
    const database = client.db(process.env.DB_NAME);
    if(await database.collection('rooms').find({userId: userId}).count() === 0){
        return false; // User does not have a room
    } else {
        return true; // User already have a room
    }
}

exports.RoomProvider = class RoomProvider {
    insertRoom = (data) => {
        let client = new MongoClient(process.env.DB_URL, { useUnifiedTopology: true });
        // const database = client.db(process.env.DB_NAME);
        isUserHaveARoom(data.userId).then(rtnBool => {
            console.log("User have a room? ".blue, rtnBool)
        });
        // return database.collection('rooms').insertOne(data).then()
    }
}