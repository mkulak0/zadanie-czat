const { TokenProvider } = require('./tokenProvider');
const { UserProvider } = require('./userProvider');

UserManager = function (app) {
    var userProvider = new UserProvider();
    var tokenProvider = new TokenProvider();

    app.put('/users', (req, res) => {
        userProvider.insertUser({
            ...req.body,
            ...req.query.isOp ? { perms: 'op' } : {}
        })
            .then(rtnVal => rtnVal ? res.status(200).json() : res.status(304).json())
            .catch((err) => console.log(err));
    });

    app.post('/users/login', (req, res) =>
        userProvider.login(req.body)
            .then(rtnVal => rtnVal ? res.send(tokenProvider.generateAccessToken(req.body.login)) : false)
    );

    app.get('/users', (req, res) => userProvider.getAllUsers().then(tab => res.json(tab)));

    app.get('/users/:id', (req, res) => userProvider.getUserByID(req.params.id)
        .then(user => user ? res.json(user) : res.send("User is not exist!")));

    app.get('/users/check', tokenProvider.authenticateToken, (req, res) => res.send("User authorized!"));

    /* Wrapping ops requests */
    /* Reconsider that */

    app.put('/ops', (req, res) => {
        res.redirect(307, '/users?isOp=true')
    })
}

exports.UserManager = UserManager;