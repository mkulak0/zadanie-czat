// const mongo = require('mongodb');
const bcrypt = require('bcrypt');
const { MongoClient, ObjectID } = require('mongodb');
const client = new MongoClient(process.env.DB_URL, { useUnifiedTopology: true });

async function createUserFromRequestData(reqData) {
    let hashedPsw = await bcrypt.hash(reqData.password, 10);
    return {
        login: reqData.login,
        perms: reqData.perms ? reqData.perms : 'user',
        password: hashedPsw
    }
}

exports.UserProvider = class UserProvider {
    insertUser = (data) =>
        createUserFromRequestData(data).then(user => {
            return client.connect().then((client) => {
                const database = client.db(process.env.DB_NAME);
                return database.collection('users').find({
                    login: user.login
                }).toArray().then((dbArray) => {
                    if (dbArray.length === 0) {
                        console.log('Creating user'.blue);
                        return database.collection('users').insertOne(user).then(() => true)
                    } else {
                        console.log('User with that login found'.red);
                        return false;
                    }
                });
            })
        })

    login = (data) => {
        return client.connect().then(() => {
            const database = client.db(process.env.DB_NAME);
            return database.collection("users")
                .findOne({ login: data.login })
                .then((value) => bcrypt.compare(data.password, value.password).then(result => result ? true : false))
        })
    }

    getAllUsers = () => client.connect().then(() => {
        const database = client.db(process.env.DB_NAME);
        return database.collection('users').find({}).toArray();
    })

    getUserByID = userId => client.connect().then(() => {
        const database = client.db(process.env.DB_NAME);
        return database.collection("users")
            .findOne({ "_id": new ObjectID(userId) }, {
                projection: { password: 0 }
            })
    })
}
